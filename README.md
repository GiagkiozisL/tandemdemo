# Tandem Demo
### was crated based on instructions of Tandem challenge procedure

The app is using mvvm pattern and following libraries
* Dagger dependency injection framework
* Retrofit REST-client for network requests
* Moshi as parser
* Coil image loader library


## Inroduction

- I was needed a 'userId' field to store the 'liked' user,
and because there wasn't any unique field to hold this information,
i ve made the decision to generate one with the following format '{page}_{index}_{user.firstName}'

- The storage of liked users ids is on shared preferences pool,
but also i could use the room database for these objects storage.

## Tests
I have also created a test for view model's accuracy
