package de.tandem.demo.di.module

import dagger.Binds
import dagger.Module
import de.tandem.demo.storage.FakeStorage
import de.tandem.demo.storage.Storage

// Overrides StorageModule in demo tests
@Module
abstract class TestStorageModule {

    // Makes Dagger provide FakeStorage when a Storage type is requested
    @Binds
    abstract fun provideStorage(storage: FakeStorage): Storage
}