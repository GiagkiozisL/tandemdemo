package de.tandem.demo.storage

import android.content.Context
import javax.inject.Inject

class SharedPreferencesStorage @Inject constructor(context: Context) : Storage {

    private val sharedPreferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    override fun setString(key: String, value: String) {
        with(sharedPreferences.edit()) {
            putString(key, value)
            apply()
        }
    }

    override fun getString(key: String): String {
        return sharedPreferences.getString(key, "")!!
    }

    override fun setList(key: String, value: Set<String>) {
        with(sharedPreferences.edit()) {
            putStringSet(key, value)
            apply()
        }
    }

    override fun getList(key: String): Set<String> {
        return sharedPreferences.getStringSet(key, setOf())!!
    }
}
