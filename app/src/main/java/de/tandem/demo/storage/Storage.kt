package de.tandem.demo.storage

interface Storage {
    fun setString(key: String, value: String)
    fun getString(key: String): String
    fun setList(key: String, value: Set<String>)
    fun getList(key: String) : Set<String>
}
