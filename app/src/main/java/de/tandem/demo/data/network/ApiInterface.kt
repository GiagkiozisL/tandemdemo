package de.tandem.demo.data.network

import de.tandem.demo.data.model.CommunityResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("/api/community_{page}.json")
    suspend fun getCommunityUsers(@Path(value = "page", encoded = true) page: Int) : Response<CommunityResponse>

}