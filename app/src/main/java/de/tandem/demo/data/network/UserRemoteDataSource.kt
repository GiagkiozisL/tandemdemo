package de.tandem.demo.data.network

import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(private val service: ApiInterface) {

    suspend fun fetchUsers(page: Int) = service.getCommunityUsers(page)
}