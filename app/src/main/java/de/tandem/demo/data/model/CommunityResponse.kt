package de.tandem.demo.data.model

data class CommunityResponse(
    val response: List<User>,
    val errorCode: String ?= null,
    val type: String
) {
}