package de.tandem.demo.data

import de.tandem.demo.data.model.CommunityResponse
import de.tandem.demo.data.network.ApiResponse
import de.tandem.demo.data.network.IRepository
import de.tandem.demo.data.network.UserRemoteDataSource
import javax.inject.Inject

class UserDataRepository @Inject constructor(private val userRemoteDataSource: UserRemoteDataSource) : IRepository {

    suspend fun getUsers(page: Int) : ApiResponse<CommunityResponse> {
        return handleRequest { userRemoteDataSource.fetchUsers(page) }
    }

}