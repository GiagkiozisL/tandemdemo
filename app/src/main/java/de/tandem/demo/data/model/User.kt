package de.tandem.demo.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class User(
    @Json(name = "topic")
    val topic: String,

    @Json(name = "firstName")
    val firstName: String,

    @Json(name = "pictureUrl")
    val pictureUrl: String,

    @Json(name = "natives")
    val natives: List<String>,

    @Json(name = "learns")
    val learns: List<String>,

    @Json(name = "referenceCnt")
    val referenceCnt: Int,

    var userId: String = "", // userId must be unique to keep 'liked' info. my format will be: {page}_{row}_{firstName} f.e 1_0_Luca
    var liked: Boolean = false
) {

}