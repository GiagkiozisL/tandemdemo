package de.tandem.demo.tools


/**
 *
 * mapInPlace is an extension method on the MutableList class that allows us to apply an operation
 * on every element of the list. The method will receive a function as a parameter,
 * and we’ll apply this function to every element in the list.
 *
 * **/
inline fun <Int> MutableList<Int>.mapInPlace(mutator: (Int, Any) -> (Int)) {
    this.forEachIndexed { index, value ->
        val changedValue = mutator(value, index)
        this[index] = changedValue
    }
}
