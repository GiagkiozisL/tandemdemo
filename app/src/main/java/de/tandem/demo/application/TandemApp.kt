package de.tandem.demo.application

import android.app.Application
import de.tandem.demo.di.component.ApplicationComponent
import de.tandem.demo.di.component.DaggerApplicationComponent

class TandemApp : Application() {

    // Instance of the AppComponent that will be used by all the Activities in the project
    val appComponent: ApplicationComponent by lazy {
        initializeComponent()
    }

    private fun initializeComponent(): ApplicationComponent {
        return DaggerApplicationComponent.factory().create(applicationContext)
    }
}