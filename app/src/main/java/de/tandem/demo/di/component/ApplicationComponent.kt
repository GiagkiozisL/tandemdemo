package de.tandem.demo.di.component

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import de.tandem.demo.community.CommunityManager
import de.tandem.demo.di.module.NetworkModule
import de.tandem.demo.di.module.ServiceModule
import de.tandem.demo.di.module.StorageModule
import javax.inject.Singleton

@Singleton
@Component(modules = [StorageModule::class, NetworkModule::class, ServiceModule::class])
interface ApplicationComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): ApplicationComponent
    }

    fun communityManager() : CommunityManager
    fun communityComponent(): CommunityComponent.Factory
}