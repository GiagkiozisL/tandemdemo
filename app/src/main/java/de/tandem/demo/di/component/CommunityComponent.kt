package de.tandem.demo.di.component


import dagger.Subcomponent

import de.tandem.demo.community.CommunityActivity

@Subcomponent
interface CommunityComponent {

    // Factory to create instances of CommunityComponent
    @Subcomponent.Factory
    interface Factory {
        fun create(): CommunityComponent
    }

    // Classes that can be injected by this Component
    fun inject(activity: CommunityActivity)
}