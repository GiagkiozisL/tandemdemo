package de.tandem.demo.di.module

import de.tandem.demo.storage.SharedPreferencesStorage
import de.tandem.demo.storage.Storage
import dagger.Binds
import dagger.Module

@Module
abstract class StorageModule {

    @Binds
    abstract fun provideStorage(storage: SharedPreferencesStorage): Storage
}