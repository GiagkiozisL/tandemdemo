package de.tandem.demo.di.module

import dagger.Module
import dagger.Provides
import de.tandem.demo.data.network.ApiInterface
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) : ApiInterface {
        return retrofit.create(ApiInterface::class.java)
    }
}