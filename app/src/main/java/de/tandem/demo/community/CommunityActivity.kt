package de.tandem.demo.community

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.tandem.demo.application.TandemApp
import de.tandem.demo.databinding.ActivityMainBinding
import kotlinx.coroutines.launch
import javax.inject.Inject


class CommunityActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var communityAdapter: CommunityAdapter

    @Inject
    lateinit var viewModel: CommunityViewModel
    lateinit var communityManager: CommunityManager

    override fun onCreate(savedInstanceState: Bundle?) {
        // Creates an instance of Communication component by grabbing the factory from the app graph
        val communityComponent = (application as TandemApp).appComponent.communityComponent().create()
        communityComponent.inject(this)

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        communityManager = (application as TandemApp).appComponent.communityManager()

        initAdapter()
        observeUsersData()
        fetchData()
    }

    private fun initAdapter() {
        val linearLayoutManager = LinearLayoutManager(this)
        val onItemLiked: (userId: String, isChecked: Boolean) -> Unit = { userId, isChecked ->
            // notify view-model
            println("$userId liked:$isChecked")
            viewModel.itemLiked(userId, isChecked)
        }

        communityAdapter = CommunityAdapter(onItemLiked)
        binding.mainRecyclerView.adapter = communityAdapter
        binding.mainRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            addItemDecoration(DividerItemDecoration(context, (layoutManager as LinearLayoutManager).orientation))
        }
        setRecyclerViewScrollListener(linearLayoutManager)
    }

    private fun fetchData() {
        lifecycleScope.launch {
            viewModel.fetchUsers()
        }
    }

    private fun observeUsersData() {
        viewModel.users.observe(this, {
            // refresh adapter
            val freshUsers = it.toMutableList()
            communityAdapter.appendItems(freshUsers)
        })
    }

    private fun setRecyclerViewScrollListener(linearLayoutManager: LinearLayoutManager) {
        binding.mainRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                /**
                 * re-fetch when user scroll to the bottom of list
                 * */
                val totalItems = communityAdapter.userList.size
                val lastVisibleIndex = linearLayoutManager.findLastVisibleItemPosition()

                if (lastVisibleIndex == totalItems - 1) {
                    // re fetch data
                    fetchData()
                }
            }
        })
    }
}