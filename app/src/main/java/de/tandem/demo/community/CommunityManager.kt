package de.tandem.demo.community

import de.tandem.demo.di.component.CommunityComponent
import de.tandem.demo.storage.Storage
import javax.inject.Inject
import javax.inject.Singleton

private const val LIKED_USERS = "liked_users"

/**
 * Handles Community lifecycle and has access in storage (shared preferences).
 * Knows user's liked ids
 *
 * Marked with @Singleton since we only one an instance of CommunicationManager in the application graph.
 */

@Singleton
class CommunityManager @Inject constructor(
    private val storage: Storage
//    ,
//    private val communityComponentFactory: CommunityComponent.Factory
) {

//    var communityComponent: CommunityComponent? = null
//        private set

    val likedUserIds: Set<String>
        get() = storage.getList(LIKED_USERS)

    init {
        setup()
    }

    private fun setup() {
//        communityComponent = communityComponentFactory.create()
    }

    fun addUser(name: String) {
        val existingUsers = storage.getList(LIKED_USERS).toMutableList()
        if (!existingUsers.contains(name)) {
            existingUsers.add(name)
            storage.setList(LIKED_USERS, existingUsers.toSet())
        }
    }

    fun removeUser(name: String) {
        val existingUsers = storage.getList(LIKED_USERS).toMutableList()
        if (existingUsers.contains(name)) {
            existingUsers.remove(name)
            storage.setList(LIKED_USERS, existingUsers.toSet())
        }
    }
}