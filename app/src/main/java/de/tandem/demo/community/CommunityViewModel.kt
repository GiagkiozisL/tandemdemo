package de.tandem.demo.community

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.tandem.demo.data.UserDataRepository
import de.tandem.demo.data.model.User
import de.tandem.demo.tools.mapInPlace
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *
 * CommunityViewModel is the ViewModel that [CommunityActivity] uses to
 * obtain user list of what to show on the screen
 *
 * **/
class CommunityViewModel @Inject constructor(
    private val userDataRepository: UserDataRepository,
    private val communityManager: CommunityManager) : ViewModel() {

    private val _users = MutableLiveData<List<User>>()
    val users: LiveData<List<User>>
        get() = _users

    var hasMoreUsers: Boolean = true
    var page: Int = 1


    fun fetchUsers() {
        if (!hasMoreUsers) {
            return
        }
        viewModelScope.launch {
            userDataRepository.getUsers(page).let {
                it.data?.let { comResponse ->
                    handleApiResult(comResponse.response)
                }
            }
        }
    }

    fun handleApiResult(fetchedUsers: List<User>) {
        hasMoreUsers = fetchedUsers.size == 20

        /**
         * i use an iteration through results
         * to construct 'userId' for every item
         * and find out if is liked from previous session
         * */
        val list = fetchedUsers.toMutableList()
        list.mapInPlace { element, index ->
            val userId = "${page}_${index}_${element.firstName}"
            element.userId = userId
            val liked = communityManager.likedUserIds.contains(userId)
            element.liked = liked
            element
        }

        page++
        _users.value = list
    }

    fun itemLiked(userId: String, isChecked: Boolean) {
        // add/ remove from storage list
        if (isChecked) communityManager.addUser(userId) else communityManager.removeUser(userId)
    }
}