package de.tandem.demo.community

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import de.tandem.demo.R
import de.tandem.demo.data.model.User

class CommunityAdapter(private val onItemLiked: (userId: String, isChecked: Boolean) -> Unit)
    : RecyclerView.Adapter<CommunityAdapter.ViewHolder>() {

    var userList = mutableListOf<User>()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val profileImageView: AppCompatImageView = itemView.findViewById(R.id.profile_image_view)
        private val userNameTxt: AppCompatTextView = itemView.findViewById(R.id.user_name)
        private val referenceCounterTxt: AppCompatTextView = itemView.findViewById(R.id.reference_count)
        private val topicTxt: AppCompatTextView = itemView.findViewById(R.id.topic_txt)
        private val nativeLangTxt: AppCompatTextView = itemView.findViewById(R.id.native_lang)
        private val learnLangTxt: AppCompatTextView = itemView.findViewById(R.id.learns_lang)
        private val freshUserBadge: CardView = itemView.findViewById(R.id.fresh_user_badge)
        val likeToggleBtn: AppCompatToggleButton = itemView.findViewById(R.id.toggle_like_btn)

        fun bind(user: User) {
            userNameTxt.text = user.firstName
            referenceCounterTxt.text = user.referenceCnt.toString()
            topicTxt.text = user.topic

            val isFreshUser = user.referenceCnt == 0
            toggleReferenceVisibility(isFreshUser)

            val hasNativesLangs = !user.natives.isNullOrEmpty()
            hasNativesLangs.let {
                nativeLangTxt.text = user.natives[0].uppercase()
            }

            val hasLearnLangs = user.learns.isNullOrEmpty()
            hasLearnLangs.let {
                learnLangTxt.text = user.learns[0].uppercase()
            }

            profileImageView.load(user.pictureUrl) {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_background)
                transformations(RoundedCornersTransformation(16f))
            }

            likeToggleBtn.isChecked = user.liked
        }

        private fun toggleReferenceVisibility(isFreshUser: Boolean) {
            if (isFreshUser) {
                freshUserBadge.visibility = View.VISIBLE
                referenceCounterTxt.visibility = View.GONE
            } else {
                freshUserBadge.visibility = View.GONE
                referenceCounterTxt.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position])
        holder.likeToggleBtn.setOnCheckedChangeListener { buttonView, isChecked ->
            if (holder.adapterPosition != RecyclerView.NO_POSITION && buttonView.isPressed) {
                userList[position].liked = isChecked
                onItemLiked.invoke(userList[position].userId, isChecked)
            }
        }
    }

    fun appendItems(freshUsers: List<User>) {
        userList.addAll(freshUsers)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}