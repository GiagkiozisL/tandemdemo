package de.tandem.demo.storage

import javax.inject.Inject

class FakeStorage @Inject constructor(): Storage {

    private val mapA = mutableMapOf<String, String>()
    private val mapB = mutableMapOf<String, Set<String>>()

    override fun setString(key: String, value: String) {
        mapA[key] = value
    }

    override fun getString(key: String): String {
        return mapA[key].orEmpty()
    }

    override fun setList(key: String, value: Set<String>) {
        mapB[key] = value
    }

    override fun getList(key: String): Set<String> {
        return mapB[key].orEmpty()
    }
}