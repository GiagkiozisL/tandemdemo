package de.tandem.demo.community



import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.tandem.demo.FileReaderHelper
import de.tandem.demo.LiveDataTestUtil
import de.tandem.demo.data.UserDataRepository
import de.tandem.demo.data.model.CommunityResponse
import de.tandem.demo.di.component.CommunityComponent
import de.tandem.demo.storage.FakeStorage
import de.tandem.demo.storage.Storage
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class CommunityVModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var userDataRepository: UserDataRepository
    private lateinit var storage: Storage
    private lateinit var communityManager: CommunityManager
    private lateinit var viewModel: CommunityViewModel
    private lateinit var moshi: Moshi

    private lateinit var adapter: JsonAdapter<CommunityResponse>

    @Before
    fun setup() {
        userDataRepository = mock(UserDataRepository::class.java)
        storage = FakeStorage()
//        val communityComponent = mock(CommunityComponent.Factory::class.java)
        communityManager = CommunityManager(storage) //, communityComponent)
        viewModel = CommunityViewModel(userDataRepository, communityManager)
    }

    @Test
    fun `test distinct user list`() {
        viewModel.itemLiked("John", true)
        viewModel.itemLiked("John", true)
        viewModel.itemLiked("John", true)
        viewModel.itemLiked("Jack", true)

        assertEquals(communityManager.likedUserIds.count(), 2)
    }

    @Test
    fun `load user list and validate`() {
        // loads a local template json
        val reader = FileReaderHelper("community.json")

        // #1 test moshi parser
        moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        adapter = moshi.adapter(CommunityResponse::class.java)
        val comResponse = adapter.fromJson(reader.content) as CommunityResponse

        // #2 validate size of list
        assertEquals(comResponse.response.size, 20)

        viewModel.handleApiResult(comResponse.response)

        // #3 ensure incremental of page and status of 'hasMoreUsers'
        assertEquals(viewModel.page, 2)
        assertTrue(viewModel.hasMoreUsers)

        // #4 ensure userID (of a random {2} user in list) constructed and isn't empty.
        assertTrue(LiveDataTestUtil.getValue(viewModel.users)[2].userId.isNotEmpty())
    }

}